module.exports = {

  wipe: {
    name: 'wipe',
    type: 'rawlist',
    message: 'A CRAWLY WITH THIS KEYWORD ALREADY EXISTS. WHAT DO YOU WANT TO DO?',
    choices: [
      'CONTINUE',
      'WIPE DATA AND RESTART'
    ]
  }
  
}