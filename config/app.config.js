module.exports = {

  port: 3000,
  sessionName: 'connect.sid',
  sessionSecret: 'mrcrawler',
  sessionCollection: 'sessions',
  sessionCookie: {
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: null
  }
  
}