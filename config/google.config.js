module.exports = {

    url: 'http://google.com',

    searchbar: 'input[name="q"]',

    buttonsearch: 'input[name="btnK"]',
    buttonnext: 'a#pnnext',

    resultOrganic: 'div.r > a:not(.fl)',
    resultAd: 'div.ads-visurl > cite'

}