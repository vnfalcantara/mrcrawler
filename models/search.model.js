const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SearchSchema = new Schema({
  
  keyword: { type: String, trim: true, lowercase: true, required: true, unique: true },

  step: { type: Number, default: 1, min: 1 },

  results: [{
    url: { type: String, trim: true },
    type: { type: String, trim: true },
    links: [{
      type: String, trim: true
    }]
  }],

  date: { type: Date, default: Date.now() }
    
});

mongoose.model('Search', SearchSchema);