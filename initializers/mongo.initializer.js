const fs = require('fs')
const color = require('../helpers/color.helper')
const mongoose = require('mongoose');

class MongoInitializer {

  constructor(configs) {
    this.modelsDir = `${__dirname}/../models`
    this.configs = configs
  }

  init() {
    return mongoose.connect(this.configs.uri, this.configs.options)
      .catch(error => {
        console.error(color.red, 'Could not connect to MongoDB!');
        console.log(color.red, error);
      })
  }

  initModel(model) {
    require(`${this.modelsDir}/${model}`)
  }

  initModels() {
    const models = fs.readdirSync(this.modelsDir)
    
    models.forEach(model => {
      require(`${this.modelsDir}/${model}`)
      console.log(color.blue, `LOADING: ${model}`)
    })
  }

}

module.exports = MongoInitializer