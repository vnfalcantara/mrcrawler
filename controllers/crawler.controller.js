const google = require('../config/google.config')
const nightmareConfig = require('../config/nightmare.config')
const promptConfig = require('../config/prompt.config')
const color = require('../helpers/color.helper')
const { prompt } = require('inquirer');
const Nightmare = require('nightmare')
const SearchService = require('../services/search.service')

const searchService = new SearchService()

class CrawlerController {

  constructor(args) {
    this.args = args
    this.nightmare = Nightmare(nightmareConfig)
    this.search = null

    this.googlePage = 0
  }

  async init() {
    this.search = await searchService.getByKeyword(this.args.keyword)
    let answer

    if (!this.search) {
      this.search = await searchService.create({keyword: this.args.keyword})
    } else if (this.search.step) {
      answer = await this.promptWipe()

      if (answer.wipe === promptConfig.wipe.choices[1]) {
        this.search = await searchService.wipe(this.search._id)
      }
    }

    this.selectStep()
  }

  async selectStep() {
    switch (this.search.step) {
      case 1:
        await this.type(this.search.keyword)
        await this.extractResults()
      case 2:
        await this.getLinks()
      case 3:
        await this.crawly()
    }
  }

  async extractResults() {
    const results = await this.getResult()
    const hasNext = await this.getNextButton() !== null
    const buttonnext = await this.getNextButton()

    this.googlePage++    
    this.search = await searchService.addResults(this.search._id, results, this.googlePage)

    if (hasNext) {
      await this.sleep(2000)
      await this.next()

      this.extractResults()
    } else {
      await this.end()
    }
  }

  async getLinks() {

  }

  async crawly() {

  }

  type(keyword) {
    console.log(color.green, 'STARTING STEP(1)')
    console.log(color.grenn, `TYPING: "${keyword}"`)

    return this.nightmare
      .goto(google.url)
      .wait(google.searchbar)
      .type(google.searchbar, keyword)
      .click(google.buttonsearch)
      .catch(error => console.error(color.red, error))
  }

  getNextButton() {
    return this.nightmare
      .evaluate(buttonNext => {
        return document.querySelector(buttonNext)
          ? document.querySelector(buttonNext).href
          : null
      }, google.buttonnext)
  }

  getResult() {
    return this.nightmare
      .wait(google.results)
      .evaluate((resultOrganic, resultAd) => {
        const organic = document.querySelectorAll(resultOrganic) || []
        const ad = document.querySelectorAll(resultAd) || []
        const links = []
        
        organic.forEach(a => links.push({url: a.href, type: 'organic'}))
        ad.forEach(a => links.push({url: a.href, type: 'ad'}))

        return links
      }, google.resultOrganic, google.resultAd)
      .catch(error => console.error(color.red, error))
  }

  next() {
    return this.nightmare
      .click(google.buttonnext)
  }

  sleep(delay) {
    return this.nightmare
      .wait(delay)
  }

  async end() {
    switch (this.search.step) {
      case 1:
        console.log(color.grenn, 'STEP(1): COMPLETED SUCCESSFULLY')
        await searchService.update(this.search._id, {step: 2})
      case 2:
        await this.getLinks()
      case 3:
        await this.crawly()
    }

    return this.nightmare
      .end()
  }

  promptWipe() {
    return prompt([
      promptConfig.wipe  
    ])
  }

}

module.exports = CrawlerController