#!/usr/bin/env node

const mongoConfig = require('./config/mongo.config')
const MongoInitializer = require('./initializers/mongo.initializer')
const program = require('commander')

const mongo = new MongoInitializer(mongoConfig)

const run = async (args) => {
	await mongo.init()
	await mongo.initModels()

	const Crawler = require('./controllers/crawler.controller')
	new Crawler(args).init()
}

program
	.version('0.0.1')
	.option('-k, --keyword [keywor]', 'desc: Keyword (REQUIRED)')
	.parse(process.argv);

	run({
		keyword: program.keyword
	})