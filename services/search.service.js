const moongose = require('mongoose')
const Search = moongose.model('Search')
const color = require('../helpers/color.helper')

class SearchService {

  constructor() {}

  create(newSearch) {
    const search = new Search(newSearch)
    
    return search.save()
      .then(search => {
        return Search.findOne({_id: search._id})
      })
  }

  getByKeyword(keyword) {
    return Search.findOne({keyword: keyword})
  }

  update(searchID, updateData) {
    return Search.findByIdAndUpdate(searchID, updateData)
      .then(search => {
        return Search.findOne({_id: search._id})
      })
  }

  countByKeyword(keyword) {
    return Search.count({keyword: keyword})
  }

  addResults(searchID, results, page) {
    return Search.findOne({_id: searchID})
      .then(search => {
        results.forEach(result => search.results.push(result))
        return search.save()
      })
      .then(search => {
        console.log(color.grenn, `STEP(1): PAGE ${page}, +${results.length} RESULTS`)
        return Search.findOne({_id: search._id})
      })
  }

  wipe(searchID) {
    return Search.findOne({_id: searchID})
      .then(search => {
        search.step = 1
        search.results = []

        return search.save()
      })
      .then(search => {
        return Search.findOne({_id: search._id})
      })
  }

}

module.exports = SearchService